import requests
import datetime
import time
#abi json to bin
url = "http://kylin.eosn.io/v1/chain/abi_json_to_bin"
payload = "{\"code\":\"eosio.token\",\"action\":\"transfer\",\"args\":{\"from\":\"redtestpeng3\",\"to\":\"redmoneytest\",\"quantity\":\"10.0000 EOS\", \"memo\":\"hi there\"}}"
headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Postman-Token': "0b072f6b-e10a-4b51-afb7-9214ce0b4592"
    }
response = requests.request("POST", url, data=payload, headers=headers)
#翻译成交易二进制流
#from redtestpeng3
#to redmoneytest
#quantity 10.0000 EOS
#memo hi there
transation_binary=response.json()["binargs"]
#print(transation_binary)

#geinfo
url = "http://kylin.eosn.io/v1/chain/get_info"
payload = ""
headers = {
    'cache-control': "no-cache",
    'Postman-Token': "0d6ee664-37ff-46c4-aeb2-be4a635ff8b1"
    }

response = requests.request("POST", url, data=payload, headers=headers)
head_block_num=response.json()['head_block_num']
print("\n============ FIRST================\n")
print(response.json())
print("\n============================\n")
chain_id=response.json()['chain_id']
print(chain_id)
url = "http://kylin.eosn.io/v1/chain/get_block"

#payload = "{\"block_num_or_id\":\"27567426\"}"
payload="{\""+"block_num_or_id"+"\""+":"+str(head_block_num)+"}"
headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Postman-Token': "40bb04b8-9e70-45df-aaef-5d145d3768f5"
    }

response = requests.request("POST", url, data=payload, headers=headers)
timestamp=response.json()['timestamp']
ref_block_prefix=response.json()['ref_block_prefix']
#print(response.text)
print(ref_block_prefix)
#sign transation
dateformat=time.mktime(time.strptime(timestamp[:-4], "%Y-%m-%dT%H:%M:%S"))+300 #交易有效期五分钟
timeArray = time.localtime(dateformat)
otherStyleTime = time.strftime("%Y-%m-%dT%H:%M:%S", timeArray)

url = "http://10.0.0.10:6699/v1/wallet/sign_transaction"

payload = "[{\r\n        \"ref_block_num\": %s,\r\n        \"ref_block_prefix\": %s,\r\n        \"expiration\": \"%s\",\r\n   \
     \"actions\": [{\r\n            \"account\": \"eosio.token\",\r\n            \"name\": \"transfer\",\r\n            \"authorization\": [{\r\n     \
                \"actor\": \"redtestpeng3\",\r\n                \"permission\": \"active\"\r\n            }],\r\n     \
                       \"data\": \"%s\"\r\n        }],\r\n     \
                          \"signatures\": []\r\n    },\r\n    [\"EOS5TSqHqJwzQpPa1F1LASZhGsmFwfUdT64kXvGifFjfGUC4tbbd1\"], \
                          \"%s\"\r\n]" %(str(head_block_num),str(ref_block_prefix),str(otherStyleTime),str(transation_binary),str(chain_id))

#payload = "[{"+"\""+"ref_block_num"+"\""+":"+str(head_block_num)+","+"\""+"ref_block_prefix"+":"+str(ref_block_prefix)
headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Postman-Token': "9da28746-13b9-4938-a3a4-cbc48a3710c3"
    }

response = requests.request("POST", url, data=payload, headers=headers)
#print(timestamp)
#print(response.json()["expiration"])
#钱包签名
Signatures=response.json()["signatures"][0]
#print(Signatures)


#push transaction
url = "http://kylin.eosn.io/v1/chain/push_transaction"

payload = "{\r\n  \"compression\": \"none\",\r\n  \"transaction\": {\r\n  \
  \"expiration\": \"%s\",\r\n    \"ref_block_num\": %s,\r\n    \"ref_block_prefix\": %s,\r\n  \
    \"context_free_actions\": [],\r\n    \"actions\": [\r\n      \
      {\r\n            \"account\": \"eosio.token\",\r\n            \"name\": \"transfer\",\r\n            \"authorization\": [\r\n                {\r\n                    \"actor\": \"redtestpeng3\",\r\n                    \"permission\": \"active\"\r\n                }\r\n            ],\r\n            \"data\": \"30d85435639593ba90b1ca5e4d2a93baa08601000000000004454f5300000000086869207468657265\"\r\n        }\r\n    ],\r\n    \"transaction_extensions\": []\r\n  },\r\n \
     \"signatures\": [\r\n        \"%s\"\r\n   ]\r\n}"%(str(otherStyleTime),str(head_block_num),str(ref_block_prefix),str(Signatures))


headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Postman-Token': "c32252ae-2036-4259-9a11-e255db8ff2df"
    }

response = requests.request("POST", url, data=payload, headers=headers)
print(response.text)


#geinfo
url = "http://kylin.eosn.io/v1/chain/get_info"
payload = ""
headers = {
    'cache-control': "no-cache",
    'Postman-Token': "0d6ee664-37ff-46c4-aeb2-be4a635ff8b1"
    }

response = requests.request("POST", url, data=payload, headers=headers)
print("\n============GETINFO LAST================\n")
print(response.json())
print("\n============================\n")